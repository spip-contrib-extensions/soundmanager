$(document).ready(function(){

	// classe laudio et id timecode sur les liens vers #t123456789
	$("a").each(function(){
		var timecode = $(this)[0].href.match(/#t(\d+)$/) ;
		if(typeof timecode !== "undefined" && timecode){
			$(this).addClass("laudio");
			$(this).attr('id', timecode[0]);
		}
	});
	
	// scroller vers le lien dans la page si timecode dans l'url d'arrivée #t10000
	var tc = window.location.hash.match(/^#t(\d+)$/);
	if(typeof tc !== "undefined" && tc){
		tc = parseInt(tc[1]);
		// trouver le lien avec ce timecode en href et y scroller
		var lien = $('a.laudio[href$="#t' + tc + '"]');
		$("a.laudio").removeClass("on");
		lien.addClass("on");
		$('body, html').animate({scrollTop: lien.offset().top - 70 }, 500);
	}

	// clic sur un lien avec timecode #t10000 ?
	$("a.laudio").on("click",function(){
		// stopper un eventuel son en cours
		if(Object.values(soundManager.sounds).length > 0){
			Object.values(soundManager.sounds).forEach(function(e){
				if(e.paused == false)
					e.stop();
			});
		}
		var timecode = $(this)[0].href.match(/t(\d+)/) ;
		if(typeof timecode !== "undefined" && timecode){
			timecode = timecode[1];
		}
		// démarrer le son du premier player
		if(typeof timecode !== "undefined" && timecode){
			console.log("demarrer le son a ", timecode);
			$(".play").eq(0).trigger("click");
		}
		// décaler le en cours de timecode
		var son = [] ;
		Object.values(soundManager.sounds).forEach(function(e){
			if(e.playState)
				son = e ;
		});
		if(typeof timecode !== "undefined" && timecode && Object.values(soundManager.sounds).length > 0){
			// son.from = timecode ;
			// la valeur de from ci dessus n'est pas prise en compte, alors on décale le son
			son.setPosition(timecode);
		}
	});
	
});
